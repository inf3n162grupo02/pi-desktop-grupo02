/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Cidade;
import model.Estado;

/**
 *
 * @author 781510023
 */
public class EstadoDAO {
    
    
        public static List<Estado> getEstados() {            // listar as cidades       ( importar LIST<E> )
        
        String sql = "SELECT * FROM estados ORDER BY nome ";            // mostra cidades em ordem alfabetica
        
        ResultSet rs = Conexao.consultar(sql);                 // chama metodo consultar em conexao
        
        List<Estado> lista = new ArrayList<>();
        
        if (rs != null) {
            
            try {
                
               while (rs.next()) {                           // continua enquanto tiver resultado
                   Estado e = new Estado();
                   e.setId(rs.getInt( "id" ));               // busca a coluna id
                   e.setNome(rs.getString( "nome" ));        //busca a coluna nome
                   lista.add(e);
               } 
                
                
            } catch (Exception e) {
           
            }
            
        }
        return lista;
        
    }
}
