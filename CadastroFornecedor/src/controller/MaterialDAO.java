/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Material;
import model.Fornecedor;

/**
 *
 * @author 181610046
 */
public class MaterialDAO {

   public void inserir(Material mat) {

        String sql = "INSERT INTO materiais "
                + " ( nome, descricao, valor, codFornecedor ) "
                + " VALUES ( "
                + " '" + mat.getNome() + "' , "
                + " '" + mat.getDescricao() + "' , "
                + "  " + mat.getValor() + " , "
                + "  " + mat.getNomeEmpresa().getId() + " ) ";

        boolean resposta = Conexao.executar(sql);
        if (resposta) {
            JOptionPane.showMessageDialog(null,
                    "Material inserido com sucesso!!!");
        }
    }

    public void editar(Material mat) {

        String sql = "UPDATE materiais SET "
                + " nome = '" + mat.getNome() + "' , "
                + " descricao = '" + mat.getDescricao()+ "' , "
                + " valor = " + mat.getValor() + " , "
                + " codFornecador = " + mat.getNomeEmpresa().getId() + " , "
                + " WHERE id = " + mat.getId();

        boolean resposta = Conexao.executar(sql);
        if (resposta) {
            JOptionPane.showMessageDialog(null,
                    "Material editado com sucesso!!!");
        }
    }

    public void excluir(Material mat) {
        String sql = "DELETE FROM materiais "
                + " WHERE id = " + mat.getId();
        boolean resposta = Conexao.executar(sql);
        if (resposta) {
            JOptionPane.showMessageDialog(null,
                    "Material excluído com sucesso!!!");
        }

    }

    public static List<Material> getMateriais() {
        List<Material> lista = new ArrayList<>();

        String sql = "SELECT m.id AS codigo, m.nome AS nomeMaterial, m.valor AS valor, "
                + " m.descricao AS descricao , f.nome AS nomeEmpresa , f.id AS codFornecedor "
                + " FROM Materiais m "
                + " INNER JOIN fornecedor f "
                + " ON f.id = m.codFornecedor ";

        ResultSet rs = Conexao.consultar(sql);

        if (rs != null) {
            try {
                while (rs.next()) {
                    Material m = new Material();
                    m.setId(rs.getInt("codigo"));
                    m.setNome(rs.getString("nome"));
                    m.setDescricao(rs.getString("descricao"));
                    m.setValor(rs.getDouble("preco"));

                    Fornecedor mat = new Fornecedor();
                    mat.setId(rs.getInt("codFornecedor"));
                    mat.setNomeEmpresa(rs.getString("fornecedor"));

                    m.setNomeEmpresa(mat);

                    lista.add(m);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.toString());
            }
        }
        return lista;
    }

    public static Material getMateriaisById(int id) {

        String sql = "SELECT m.id AS codigo, m.nome AS nomeMaterial, m.valor AS valor, "
                + " m.descricao AS descricao , f.nome AS nomeEmpresa , f.id AS codFornecedor "
                + " FROM Materiais m "
                + " INNER JOIN fornecedor f "
                + " ON f.id = m.codFornecedor ";

        ResultSet rs = Conexao.consultar(sql);

        if (rs != null) {
            try {

                rs.next();

                Material m = new Material();
                m.setId(rs.getInt("codigo"));
                m.setNome(rs.getString("nome"));
                m.setDescricao(rs.getString("descricao"));
                m.setValor(rs.getDouble("preco"));

                Fornecedor mat = new Fornecedor();
                mat.setId(rs.getInt("codFornecedor"));
                mat.setNomeEmpresa(rs.getString("fornecedor"));

                m.setNomeEmpresa(mat);

                return m;

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.toString());
                return null;
            }

        } else {
            return null;
        }
    }
}
