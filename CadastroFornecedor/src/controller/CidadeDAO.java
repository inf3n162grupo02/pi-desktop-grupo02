/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Cidade;
import model.Estado;

/**
 *
 * @author 781510023
 */
public class CidadeDAO {

    Estado est = new Estado();

    public static List<Cidade> getCidades(int id) {            // listar as cidades       ( importar LIST<E> )

        String sql = "SELECT * FROM cidades WHERE codEstado = " + id;     // mostra cidades com ID do estado

        ResultSet rs = Conexao.consultar(sql);                 // chama metodo consultar em conexao

        List<Cidade> lista = new ArrayList<>();

        if (rs != null) {

            try {

                while (rs.next()) {                           // continua enquanto tiver resultado
                    Cidade c = new Cidade();
                    c.setId(rs.getInt("id"));               // busca a coluna id
                    c.setNome(rs.getString("nome"));        //busca a coluna nome
                    lista.add(c);
                }

            } catch (Exception e) {

            }

        }
        return lista;

    }

}
