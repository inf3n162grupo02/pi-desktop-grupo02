/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Cidade;
import model.Estado;
import model.Fornecedor;

/**
 *
 * @author 181610046
 */
public class FornecedorDAO {

    public void inserir(Fornecedor forne) {


      


        String sql = "INSERT INTO fornecedores (nomeEmpresa, cnpj, logradouro, numero, complemento, bairro, cep, telefone, email, codCidade )"
                + " VALUES ( "
                + " '" + forne.getNomeEmpresa() + "' , "
                + " '" + forne.getCnpj() + "' , "
                + " '" + forne.getLogradouro() + "' , "
                + " '" + forne.getNumero() + "' , "
                + " '" + forne.getComplemento() + "', "
                + " '" + forne.getBairro() + "', "
                + " '" + forne.getCep() + "' , "
                + " '" + forne.getTelefone() + "',"
                + " '" + forne.getEmail() + "',"

                + "  " + forne.getCidade().getId() + " ) ";
        
        boolean resposta = Conexao.executar(sql);
        if (resposta) {
            JOptionPane.showMessageDialog(null,
                    "Fornecedor inserido com sucesso!!!");
        }
        
         
    }

    
    public void editar(Fornecedor forne) {

        String sql = "UPDATE fornecedores SET "
                + " nomeEmpresa = '" + forne.getNomeEmpresa() + "' , "
                + " cnpj = " + forne.getCnpj() + " , "
                + " logradouro = " + forne.getLogradouro() + " , "
                + " numero = " + forne.getNumero() + " , "
                + " complemento = " + forne.getComplemento() + " , "
                + " bairro = " + forne.getBairro() + " , "
                + " cep = " + forne.getCep() + " , "
                + " telefone = " + forne.getTelefone() + " , "
                + " email = " + forne.getEmail() + " , "
                + " codCidade = " + forne.getCidade().getId() + " , "
                + " WHERE id = " + forne.getId();

        boolean resposta = Conexao.executar(sql);
        if (resposta) {
            JOptionPane.showMessageDialog(null,
                    "Fornecedor editado com sucesso!!!");
        }
    }

    public void excluir(Fornecedor forne) {
        String sql = "DELETE FROM fornecedores "
                + " WHERE id = " + forne.getId();
        boolean resposta = Conexao.executar(sql);
        if (resposta) {
            JOptionPane.showMessageDialog(null,
                    "Fornecedor excluído com sucesso!!!");
        }


    }

    public static List<Fornecedor> getFornecedores() {

        List<Fornecedor> lista = new ArrayList<>();

        String sql = "SELECT f.id AS codigo , f.nomeEmpresa AS nome, f.cnpj AS cnpj, f.telefone AS tel, "
                + " f.email AS email, c.nome AS cidade, c.id AS codCid, e.id AS codEstado, e.nome AS estado "
                + " FROM fornecedores f "
                + " INNER JOIN cidades c ON c.id = f.codCidade "
                + " INNER JOIN estados e ON e.id = c.codEstado ";                   // consultar clientes e cidades com o INNER JOIN

        ResultSet rs = Conexao.consultar(sql);

        if (rs != null) {

            try {

                while (rs.next()) {

                    Fornecedor f = new Fornecedor();
                    f.setId(rs.getInt("codigo"));
                    f.setNomeEmpresa(rs.getString("nome"));
                    f.setCnpj(rs.getString("cnpj"));
                    f.setTelefone(rs.getString("tel"));
                    f.setEmail(rs.getString("email"));

                    Cidade cid = new Cidade();
                    cid.setId(rs.getInt("codCid"));
                    cid.setNome(rs.getString("cidade"));
                    
                    Estado est = new Estado();
                    est.setId( rs.getInt("codEstado"));
                    est.setNome(rs.getString("estado"));
                    
                    cid.setEstado(est);

                    f.setCidade(cid);

                    
                    
                    
                    lista.add(f);

                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.toString());
            }
        }

        return lista;
    }

    public static Fornecedor getFornecedoresById(int id) {

        String sql = "SELECT f.id AS codigo , f.nomeEmpresa AS nome, f.cnpj AS cnpj, f.telefone AS tel, f.email AS email, c.nome AS cidade, c.id AS codCid FROM fornecedores f , enderecos end INNER JOIN cidades c ON c.id = end.codCidade ";                   // consultar clientes e cidades com o INNER JOIN

        ResultSet rs = Conexao.consultar(sql);

        if (rs != null) {

            try {

                rs.next();

                Fornecedor f = new Fornecedor();
                f.setId(rs.getInt("codigo"));
                f.setNomeEmpresa(rs.getString("nome"));
                f.setCnpj(rs.getString("cnpj"));
                f.setTelefone(rs.getString("tel"));
                f.setEmail(rs.getString("email"));

                Cidade cid = new Cidade();
                cid.setId(rs.getInt("codCid"));
                cid.setNome(rs.getString("cidade"));

                f.setCidade(cid);

                return f;

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.toString());
                return null;
            }
        } else {
            return null;

        }
    }
}
