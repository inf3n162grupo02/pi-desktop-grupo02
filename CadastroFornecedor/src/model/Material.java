/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 181610046
 */
public class Material {

    private int id;
    private String nome, descricao;
    private double valor;
    private Fornecedor nomeEmpresa;
    

    public Material(int id, String nome, String descricao, double valor, Fornecedor nomeEmpresa) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.nomeEmpresa = nomeEmpresa;
        
    }

    public Material(int id, String nome, double valor, Fornecedor nomeEmpresa) {
        this.id = id;
        this.nome = nome;
        this.valor = valor;
        this.nomeEmpresa = nomeEmpresa;
    }

    public Material() {
    }

    public Material(int id, double valor) {
        this.id = id;
        this.valor = valor;
        
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public Fornecedor getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(Fornecedor nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return nome;
    }

}
