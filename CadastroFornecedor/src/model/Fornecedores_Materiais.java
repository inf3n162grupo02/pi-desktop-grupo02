/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 781510023
 */
public class Fornecedores_Materiais {
    
    
    private int id;
    private int codFornecedores;
    private int codMateriais;

    public Fornecedores_Materiais() {
    }

    public Fornecedores_Materiais( int codFornecedores, int codMateriais) {
        this.codFornecedores = codFornecedores;
        this.codMateriais = codMateriais;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodFornecedores() {
        return codFornecedores;
    }

    public void setCodFornecedores(int codFornecedores) {
        this.codFornecedores = codFornecedores;
    }

    public int getCodMateriais() {
        return codMateriais;
    }

    public void setCodMateriais(int codMateriais) {
        this.codMateriais = codMateriais;
    }
    
    
    
}
